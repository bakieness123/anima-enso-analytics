<?php

namespace Yadda\Enso\Analytics;

use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Analytics\Contracts\Controller as ControllerContract;
use Yadda\Enso\Analytics\Controllers\AnalyticsController as ControllerConcrete;
use Yadda\Enso\Facades\EnsoMenu;

class EnsoAnalyticsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'enso-analytics');

        if (config('analytics.view_id')) {
            EnsoMenu::addItem(
                [
                    'route' => ['admin.analytics.index'],
                    'label' => 'Analytics',
                    'icon' => 'fa fa-line-chart',
                    'order' => 90,
                ]
            );
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ControllerContract::class, ControllerConcrete::class);
    }
}
