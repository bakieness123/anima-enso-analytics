@if(!empty(EnsoSettings::get('google-tag-manager-code')))
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ EnsoSettings::get('google-tag-manager-code') }}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
@endif