<?php

use Yadda\Enso\Analytics\Contracts\Controller as AnalyticsController;

if (config('analytics.view_id')) {
    Route::group(['middleware' => ['web', 'enso']], function () {
        Route::get('admin/analytics', '\Yadda\Enso\Analytics\Controllers\AnalyticsController@index')
            ->name('admin.analytics.index');
    });
}
